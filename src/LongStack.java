import java.util.*;

public class LongStack {

   public static void main (String[] argum) {
   }

   private LinkedList<Long> stack;

   LongStack() {
      stack = new LinkedList<>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack tmp = new LongStack();
      if (stEmpty()) {
         throw new CloneNotSupportedException();
      }
      for (Long aLong : stack) {
         tmp.stack.addLast(aLong);
      }
      return tmp;
   }

   public boolean stEmpty() {
      return stack.size() <= 0;
   }

   public void push (long a) {
      stack.push(a);
   }

   public long pop() {
      try {
         return stack.pop();
      } catch (NoSuchElementException X) {
         throw new NoSuchElementException("Not enough numbers in stack.");
      }
   }

   public void op (String s) {
      try {
         long x;
         long y;
         switch (s) {
            case "+":
               x = stack.pop();
               y = stack.pop();
               stack.push(y + x);
               break;
            case "-":
               x = stack.pop();
               y = stack.pop();
               stack.push(y - x);
               break;
            case "*":
               x = stack.pop();
               y = stack.pop();
               stack.push(y * x);
               break;
            case "/":
               x = stack.pop();
               y = stack.pop();
               stack.push(y / x);
               break;
            case "SWAP":
               x = stack.pop();
               y = stack.pop();
               stack.push(x);
               stack.push(y);
               break;
            case "ROT":
               x = stack.pop();
               y = stack.pop();
               var z = stack.pop();
               stack.push(y);
               stack.push(x);
               stack.push(z);
               break;
            default:
               stack.push(Long.parseLong(s));
               break;
         }
      } catch (IllegalArgumentException X) {
         throw new IllegalArgumentException("Illegal symbol in stack: " + s);
      } catch (NoSuchElementException X) {
         throw new NoSuchElementException("Not enough numbers in stack.");
      }
   }


  
   public long tos() {
      if (stEmpty()) {
         throw new NullPointerException("Empty stack");
      }
      return stack.peek();
   }

   @Override
   public boolean equals (Object o) {
      return ((LongStack) o).stack.equals(stack);
   }

   @Override
   public String toString() {
      if (stack.isEmpty())
         return "empty";
      StringBuilder b = new StringBuilder();
      for (int i = stack.size() - 1; 0 <= i ; i--) {
         b.append(stack.get(i)).append(" ");
      }
      return b.toString();
   }

   public static long interpret (String pol) {
      if (pol.equals("")) { throw new IllegalArgumentException("Empty expression."); }

      LongStack data = new LongStack();

      String[] stringData = pol.split("\\s+");

      for (String symbol : stringData) {
          if (!symbol.equals("")) {
             try {
                data.op(symbol);
             } catch (IllegalArgumentException X) {
                throw new RuntimeException("Illegal symbol: " + symbol + " in expression: '" + pol + "'");
             } catch (NoSuchElementException X) {
                throw new RuntimeException("Cannot perform " + symbol + " in expression '" + pol + "'");
             }
          }
      }
      if (data.stack.size() == 1) {
         return data.tos();
      } else {
         throw new RuntimeException("Too many numbers in expression: '" + pol + "'");
      }

   }

}

